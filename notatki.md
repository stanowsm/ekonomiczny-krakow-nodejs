## Instalacje

node -v
v16.13.1

npm -v
6.14.6

git --version
git version 2.31.1.windows.1

// https://git-scm.com/download/mac
brew install git

code -v
1.63.2

docker --version
Docker version 20.10.12, build e91ed57
docker ps

docker run -d -p 80:80 docker/getting-started
visit http://localhost/
docker rm -f e0dddc835727

https://golb.hplar.ch/2020/05/docker-windows-home-2004.html

## GIT Syncing changes from upstream

https://bitbucket.org/ev45ive/ekonomiczny-krakow-nodejs.git

Lewa strona => Plusik (+) => FOrk this repository => Public

Lewa strona => Avatar => Settings => App Passwords

<!-- Clone do katalogu ekonomiczny-krakow-nodejs -->

git clone https://bitbucket.org/<WASZ_LOGIN>/ekonomiczny-krakow-nodejs.git ekonomiczny-krakow-nodejs

<!-- lub do biezacego katalogu -->

git clone https://bitbucket.org/<WASZ_LOGIN>/ekonomiczny-krakow-nodejs.git .

cd ekonomiczny-krakow-nodejs

git remote add trener https://bitbucket.org/ev45ive/ekonomiczny-krakow-nodejs.git

<!-- pobierz najnowze zmiany od trenera (tzw. upstream)  -->

git pull trener master

## Git proxy

git config --global --unset http.proxy
git config --global --unset https.proxy

## NPM install global

npm i -g http-server

C:\Users\PC\AppData\Roaming\npm\http-server -> C:\Users\PC\AppData\Roaming\npm\node_modules\http-server\bin\http-server

- http-server@14.1.0
  added 18 packages from 14 contributors, removed 2 packages and updated 8 packages in 3.083s

http-server -p 8080

### Linux / Mac global packages no sudo

https://stackoverflow.com/questions/14803978/npm-global-path-prefix

~/.npmrc :
npm config set prefix /Users/[your username]/.npm-packages/bin

.bash_profile :
export PATH=$PATH:/Users/[your username]/.npm-packages/bin

## Package.json / npm init -y

npm init -y
Wrote to C:\Projects\szkolenia\nodejs-ekonomiczny-krakow\examples\cli-comander\package.json:

{
"name": "cli-comander",
"version": "1.0.0",
"description": "Arg parser CLI tool",
"main": "index.js",
"scripts": {
"test": "echo \"Error: no test specified\" && exit 1"
},
"author": "",
"license": "ISC",
"keywords": []
}

## Npm Execute

npx rimraf -r node_modules/

## SemVer

https://semver.org/
https://semver.npmjs.com/

npm outdated

Package Current Wanted Latest Location  
commander 8.3.0 8.3.0 9.0.0 cli-comander

npm update
npm install commander@^9.0.0
npm install commander@latest

## Debugger

node --inspect ./index.js

<!-- lub: -->

node --inspect-brk ./index.js
Debugger listening on ws://127.0.0.1:9229/d9023bc0-4594-4cd0-acae-c1dbd95b6614
For help, see: https://nodejs.org/en/docs/inspector

"scripts": {
"start": "node ./index.js",
"serve": "nodemon ./index.js",
"start:debug": "node --inspect-brk ./index.js",
"serve:debug": "nodemon --inspect-brk ./index.js",
},

debugger;

## Express template engines

https://expressjs.com/en/resources/template-engines.html
https://expressjs.com/en/guide/using-template-engines.html

https://github.com/mde/ejs/wiki/Using-EJS-with-Express
https://ejs.co/#install

## 12 factor

https://12factor.net/pl/
https://expressjs.com/en/advanced/best-practice-performance.html

## Cache control

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control

## MongoDB Docker

https://www.mongodb.com/

https://www.mongodb.com/try

docker run --name mymongo -d -p 27017:27017 mongo

docker ps

docker exec -it mymongo mongo mongodb://localhost:27017/uniwersytet

db.users.insert({'name':'Test user'})

61fd3374729e84f51871be3a

```js
db.users
  .aggregate([
    {
      $match: {
        "orders.0": { $exists: true },
      },
    },
    {
      $unwind: "$orders",
    },
  ])
  .pretty();
```

<!-- update({'category.id': 111 }, {'category.name': 'New name' }) -->

ticket : {
  id:123,
  accteped:{
    user: 123
  }
  reservations:[
    <!-- {usr:123, date:..., statu:'nowa'}, -->
    {usr:234, date:..., statu:'rejected'},
    {usr:123, date:..., statu:'accepted'},
  ]
}