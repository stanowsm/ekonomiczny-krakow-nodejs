var { Router } = require("express");
let multer = require('multer')
let fs = require('fs')
let path = require('path')


const routes = Router()

routes.use((req, res, next) => {
    res.locals.baseUrl = req.baseUrl
    next()
})

routes.get("/", function (req, res) {
    res.render('pages/index', {
        // user: req.user
    })
});

routes.get('/visits', function (req, res, next) {
    if (req.session.views) {
        req.session.views++
        res.send('Welcome back! Its your ' + req.session.views + ' visit!')
    } else {
        req.session.views = 1
        res.send('Hello! Its your first time with us')
    }
})

routes.get("/signin", function (req, res) {
    res.render('pages/signin', { email: '', message: '' })
});

routes.get("/signup", function (req, res) {
    res.render('pages/signup')
});

routes.get("/contact", function (req, res) {
    res.render('pages/contact', { email: '', message: '', errors: [] })
});


const upload = multer({
    dest: './public/uploads/',
})

routes.post("/contact", [
    upload.single('file')
], function (req, res, next) {
    const { email = '', message = '' } = req.body
    
    let errors = []
    if (!email || !message) {
        errors.push('Invalid data')
    }

    if (errors.length) {
        return res.render('pages/contact', { email, message, errors })
    }

    if (req.file) {
        const file = req.file
        fs.rename(file.path, path.join(file.destination, file.originalname), (err, result) => {
            if (err) {
                // res.setStatus(500).send('Error')
                return next(new Error('Cannot rename file'))
            }
            res.redirect('/pages/contact')
        })
    }

});

// routes.post("/contact", function (req, res) {
//     const { email, message } = req.query
// })


module.exports = routes
