const { ObjectId } = require('bson')
let crypto = require('crypto')
const client = require('../config/mongo')

class UsersRepository {

    constructor(events) {
        // events.on(...)
        this.events = events
    }

    usersCol = client.db('uniwersytet').collection('users')

    async getAll() {
        const cursor = this.usersCol.find({}, {
            limit: 10,
            projection: {
                password: 0
            }
        })
        // for await (let doc of cursor) {
        //     console.log((doc));
        // }
        const result = await cursor.toArray()

        return result
    }

    async findByName(name) {
        return this.usersCol.find({
            name: name
        }, {
            limit: 10,
            projection: {
                password: 0
            }
        }).toArray()
    }

    async findById(id) {
        return this.usersCol.findOne({ _id: new ObjectId(id) }, {
            projection: {
                password: 0
            }
        })
    }

    async create(draft) {
        this.events.emit('product:created', draft)

        return this.usersCol.insertOne({ 
            name: draft.name,
            orders:[
                { name:'Product 123' },
                { name:'Product 234' },
            ]
        }, {
            projection: {
                password: 0
            }
        })
    }

    async update(draft) {
        if (!draft.id) { throw new Error('Cannot update without ID') }

        await this.usersCol.findOneAndUpdate({
            _id: new ObjectId(draft.id)
        }, {
            $set: {
                name: draft.name
            },
        }, {
            projection: {
                password: 0
            },
            returnDocument: 'after'
        })
        return draft
    }

    async delete(id) {
        await this.usersCol.deleteOne({ _id: new ObjectId(id) })
        return id
    }
}


module.exports = UsersRepository