const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  // completer(linePartial, callback) { callback(null, [['123'], linePartial]); })
});

rl.prompt();

rl.on("line", (line) => {
  console.log(`Received: ${line}`);
});

// or

// rl.question("What is your ultimate question? ", (answer) => {
//   console.log(`42`);
// //   rl.close();
// });