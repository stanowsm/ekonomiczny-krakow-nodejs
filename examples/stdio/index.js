// Dont affect DISK:
const path = require('path')

// DOES affect DISK:
const fs = require('fs')

let data = Buffer.alloc(100)
data.write('Testowa linia w pliku \r\n', 0)

console.log(process.memoryUsage().heapUsed / 1024 / 2024);
const fd = fs.openSync('./text.txt', 'a') //, { encoding: 'utf-8'})

for (let i = 0; i < 1_000_000; i++) {
    fs.writeFileSync(fd, data.slice(20), { flag: 'a' })
}

console.log(process.memoryUsage().heapUsed / 1024 / 2024);

// fs.writeFileSync('text.txt', data, {}) //, { encoding: 'utf-8'})
console.log('Finished');

// RangeError: Invalid string length

// fs.readFile
// const stdIn = fs.readFileSync(process.stdin.fd) //, { encoding: 'utf-8'})
// const stdIn = fs.readFileSync('text.txt') //, { encoding: 'utf-8'})
// const file = stdIn.toString('utf-8')

// console.log('Content: ' + file);